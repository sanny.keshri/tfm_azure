resource "azurerm_resource_group" "TFM_example" {
  name     = "TFM_resource"
  location = "East US"
}

resource "azurerm_network_security_group" "TFM_NSG" {
  name                = "NSG1"
  location            = azurerm_resource_group.TFM_example.location
  resource_group_name = azurerm_resource_group.TFM_example.name

  security_rule {
    name                       = "test123"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = "Production"
  }
}