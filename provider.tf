terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.107.0"
    }
  }
}

provider "azurerm" {
  # Configuration options
features {}

#  client_id       = var.ARM_CLIENT_ID
#  client_secret   = var.ARM_CLIENT_SECRET
#  tenant_id       = var.ARM_SUBSCRIPTION_ID
#  subscription_id = var.ARM_TENANT_ID
}